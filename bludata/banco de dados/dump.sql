/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.27 : Database - clienteblu
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_cliente` varchar(150) DEFAULT NULL,
  `uf_id` int(11) DEFAULT NULL,
  `rg` varchar(15) DEFAULT NULL,
  `cpf` char(15) DEFAULT NULL,
  `data_nasc` date DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cliente_id`),
  KEY `FK_uf_if_cli` (`uf_id`),
  CONSTRAINT `FK_uf_if_cli` FOREIGN KEY (`uf_id`) REFERENCES `uf` (`uf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `cliente` */

/*Table structure for table `configuracao` */

DROP TABLE IF EXISTS `configuracao`;

CREATE TABLE `configuracao` (
  `configuracao_id` int(11) NOT NULL AUTO_INCREMENT,
  `uf_id` int(11) DEFAULT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`configuracao_id`),
  KEY `FK_conf_uf` (`uf_id`),
  CONSTRAINT `FK_conf_uf` FOREIGN KEY (`uf_id`) REFERENCES `uf` (`uf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `configuracao` */

/*Table structure for table `telefone` */

DROP TABLE IF EXISTS `telefone`;

CREATE TABLE `telefone` (
  `telefone_id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) DEFAULT NULL,
  `numero` char(15) DEFAULT NULL,
  `ddd` char(2) DEFAULT NULL,
  PRIMARY KEY (`telefone_id`),
  KEY `FK_cliente_id_tel` (`cliente_id`),
  CONSTRAINT `FK_cliente_id_tel` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `telefone` */

/*Table structure for table `uf` */

DROP TABLE IF EXISTS `uf`;

CREATE TABLE `uf` (
  `uf_id` int(11) NOT NULL AUTO_INCREMENT,
  `uf` char(2) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`uf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `uf` */

insert  into `uf`(`uf_id`,`uf`,`estado`) values (1,'SC','Santa Catarina'),(2,'PR','Paraná');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
