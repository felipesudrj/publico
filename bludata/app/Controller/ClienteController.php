<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ClienteController extends AppController {

    public $uses = array('Configuracao', 'Uf', 'Cliente', 'Telefone');
    public $helpers = array('FilterResults.FilterForm');
    public $components = array(
        'FilterResults.FilterResults' => array(
            'auto' => array(
                'paginate' => false,
                'explode' => true, //recomendados
            ),
            'explode' => array(
                'character' => ' ',
                'concatenate' => 'AND',
            )
        )
    );

    public function beforeRender() {
        parent::beforeRender();
        $configuracao = $this->Configuracao->find('count');

        if (empty($configuracao)) {
            $this->Session->setFlash('Antes de utilizar o sistema defina uma configuração de UF');
            $this->redirect('/configuracao');
        }
    }

    public function index() {
        $this->redirect(array('action' => 'novo'));
    }

    public function detalhes() {
        $configuracao = $this->Configuracao->find('first');
        $config = $configuracao['Configuracao']['uf_id'];
        $this->set('conf', $config);
        /** PEGAR TODOS OS CLIENTES DA CONFIGURAÇAO */
        $this->Paginator = array('conditions' => array('uf_id' => $config), 'limit' => '5');
        $clientes = $this->Paginate('Cliente');
        $this->set('clientes', $clientes);
    }

    public function remove($id) {
        $configuracao = $this->Configuracao->find('first');
        $config = $configuracao['Configuracao']['uf_id'];
        $this->set('conf', $config);
        $cliente = $this->Cliente->find('first', array('conditions' => array('uf_id' => $config, 'cliente_id' => $id)));
        $this->set('cliente', $cliente);

        if ($this->request->is('post')) {
            $cliente_id = $this->request->data['cliente_id'];


            $this->Cliente->delete($cliente_id);
            $this->redirect(array('action' => 'lista'));
        }
    }

    public function lista() {
        $configuracao = $this->Configuracao->find('first');
        $config = $configuracao['Configuracao']['uf_id'];
        $this->set('conf', $config);
        /** PEGAR TODOS OS CLIENTES DA CONFIGURAÇAO */
        if ($this->request->is('post')) {
            /** Tratar datas */
            if (!empty($this->request->data['filter']['data_nasc'])) {
                $novaData = DateTime::createFromFormat('d/m/Y', trim($this->request->data['filter']['data_nasc']));
                $this->request->data['filter']['data_nasc'] = $novaData->format('Y-m-d');
            }

            if (!empty($this->request->data['filter']['data_cadastro'])) {
                $novaData = DateTime::createFromFormat('d/m/Y', trim($this->request->data['filter']['data_cadastro']));
                $this->request->data['filter']['data_cadastro'] = $novaData->format('Y-m-d');
            }
        }

        $optionsFilter = array(
            'nome_cliente' => array(
                'Cliente.nome_cliente' => array(
                    'operator' => 'Like',
                )
                
            ),
            'data_nasc' => array(
                'Cliente.data_nasc' => array(
                    'operator' => '=',
                ),
            ),
            'data_cadastro' => array(
                'Cliente.data_cadastro' => array(
                     'operator' => 'Like'
                ),
            ),

                

            
        );

        $this->FilterResults->addFilters($optionsFilter);
        
        
        
        $this->FilterResults->setPaginate('limit', 10);

        
        
        $conditions = $this->FilterResults->getConditions();
        $conditions[] =  array('Cliente.uf_id'=>$config);
        
        $this->FilterResults->setPaginate('conditions', $conditions);
      

        $clientes = $this->Paginate('Cliente');
        $this->set('clientes', $clientes);
    }

    public function removeFone() {
        $this->layout = null;
        $telefone_id = $this->request->data['id_fone'];

        if ($this->Telefone->delete($telefone_id)) {
            $retorno['status'] = true;
        } else {
            $retorno['status'] = false;
        };

        echo json_encode($retorno);

        die;
    }

    public function novo($id = null) {


        $configuracao = $this->Configuracao->find('first');
        $config = $configuracao['Configuracao']['uf_id'];
        $this->set('conf', $config);




        if ($this->request->is('post')) {

            $form = $this->request->data;




            if (!$this->CamposPreenchidos($form)) {

                $this->Session->setFlash('Preencha pelo menos um dos campos com informações sobre o cliente.');
                $this->redirect(array('controller' => 'cliente', 'action' => 'novo'));
            }

            /* FORMATAR CPF E INSERIR CODIGO DE UF BASE */
            $form['Cliente']['cpf'] = str_replace(array('.', '-'), '', $form['Cliente']['cpf']);
            $form['Cliente']['uf_id'] = $config;


            /** FORMATAR PADRÃO DE DATA */
            $novaData = DateTime::createFromFormat('d/m/Y', $form['Cliente']['data_nasc']);
            $form['Cliente']['data_nasc'] = $novaData->format('Y-m-d');


            /** VERIFICAR SE CLIENTE JÁ EXISTE CASO NAO SEJA UMA EDICAO */
            if (!isset($form['Cliente']['cliente_id'])) {

                $cadastro = $this->Cliente->findBycpf($form['Cliente']['cpf']);
                if ($cadastro) {
                    $this->Session->setFlash('Esse usuário já está cadastrado no sistema');
                    $this->redirect(array('controller' => 'cliente', 'action' => 'novo', $cadastro['Cliente']['cliente_id']));
                };
            }


            if ($this->Cliente->saveAll($form)) {

                if (isset($form['Cliente']['cliente_id'])) {
                    $this->Session->setFlash('Editado com sucesso');
                    $this->redirect(array('controller' => 'cliente', 'action' => 'lista'));
                } else {
                    $this->Session->setFlash('Salvo com sucesso');
                    $this->redirect(array('controller' => 'cliente', 'action' => 'novo'));
                }
            }
        } else {
            if ($id != null) {
                $cliente = $this->Cliente->findBycliente_id($id);
                $cliente['Cliente']['data_nasc'] = date('d/m/Y', strtotime($cliente['Cliente']['data_nasc']));
                $this->data = $cliente;
                $this->set('cliente_id', $id);
            }
        }
    }

    private function CamposPreenchidos($form) {
        /** VALIDAR PREENCHIMENTO DE CAMPOS */
        $totalCampos = count($form['Cliente']);
        $camposEmBranco = 0;
        foreach ($form['Cliente'] as $validarCampos => $valor) {
            if (empty($valor)) {
                $camposEmBranco++;
            }
        }

        if ($camposEmBranco == $totalCampos) {
            return false;
        } else {
            return true;
        }
        die;
    }

}
