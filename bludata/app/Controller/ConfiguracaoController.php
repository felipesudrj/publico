<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ConfiguracaoController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Configuracao', 'Uf');

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function index() {





        if ($this->request->is('post')) {

            /** PAGAR QUALQUER CONFIGURAÇÃO ATUAL E INCLUIR A NOVA */
            $form = $this->request->data;

            $this->Configuracao->deleteAll(array('uf_id !=' => null));
            $this->Configuracao->save($form);
            
            $configuracao = $this->Configuracao->find('first', array('fields' => array('uf_id')));
            $this->data = $configuracao;
            $uf = $this->Uf->find('list', array('fields' => array('uf_id', 'uf')));
            $this->set('uf', $uf);
            
            $this->set('confirma',true);
            
        } else {


            $configuracao = $this->Configuracao->find('count');

            if (empty($configuracao)) {
                $uf = $this->Uf->find('list', array('fields' => array('uf_id', 'uf')));
                $this->set('uf', $uf);
            } else {
                $configuracao = $this->Configuracao->find('first', array('fields' => array('uf_id')));

                $this->data = $configuracao;

                $uf = $this->Uf->find('list', array('fields' => array('uf_id', 'uf')));
                $this->set('uf', $uf);
            }
        }
    }

}
