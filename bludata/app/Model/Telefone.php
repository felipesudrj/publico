<?php
App::uses('AppModel', 'Model');
/**
 * BannerHome Model
 *
 */
class Telefone extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'telefone';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'telefone_id';
        
        public function beforeSave($options = array()) {
           
            parent::beforeSave($options);
            
            $numero = str_replace(array('(',')','-'),'',$this->data['Telefone']['numero']);
            $this->data['Telefone']['numero'] = substr($numero,2);
            $this->data['Telefone']['ddd'] = substr($numero,0,2);
          
            return true;
        }
        
}
