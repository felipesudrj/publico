<?php
App::uses('AppModel', 'Model');
/**
 * BannerHome Model
 *
 */
class Configuracao extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'configuracao';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'configuracao_id';

}