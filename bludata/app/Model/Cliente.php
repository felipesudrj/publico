<?php
App::uses('AppModel', 'Model');
/**
 * BannerHome Model
 *
 */
class Cliente extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cliente';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'cliente_id';
        
        public $hasMany = array(
            'Telefone' => array(
                 'className' => 'Telefone',
                 'foreignKey' => 'cliente_id'
                )
        );

}