<?php
App::uses('AppModel', 'Model');
/**
 * BannerHome Model
 *
 */
class Uf extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'uf';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'uf_id';

}