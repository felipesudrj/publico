﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cadastro de clientes</title>
	<!-- BOOTSTRAP STYLES-->
     <?php echo $this->Html->css(array('bootstrap','font-awesome','morris-0.4.3.min','custom'));?>
    <?php echo $this->Html->script(array('Jquery.min','jquery-1.10.2','bootstrap.min','jquery.metisMenu','custom')) ?>
    <?php echo $this->fetch('JavaScriptPaginas');?>
     
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Barra de navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">Painel admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> Último acesso em 30/07/2014 &nbsp; <a href="#" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                  
                    <?php echo $this->Html->image('find_user.png', array('alt' => 'Usuario'));?>
					</li>
				
					
                    <li>
                        <a class="active-menu"  href="/"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                   
                    <li>
                        
                        <a  href="/configuracao"><i class="fa fa-qrcode fa-3x"></i> Configuração</a>
                    </li>
			
                    <li  >
                        <a  href="/cliente/novo"><i class="fa fa-edit fa-3x"></i>Novo Cadastro </a>
                    </li>				
					
					                   
                    <li>
                        <a href="#"><i class="fa fa-table fa-3x"></i> Relatório de clientes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           
                            
                            <li>
                                <a href="#">Lista de Clientes<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="/cliente/lista/">Avançado</a>
                                    </li>
                                    <li>
                                        <a href="/cliente/detalhes">Simples</a>
                                    </li>
                                    

                                </ul>
                               
                            </li>
                        </ul>
                      </li>  
                      	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <?php echo $this->fetch('content');?>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
   
    

    
   
</body>
</html>
