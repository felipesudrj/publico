<?php if(isset($confirma)){?>

<script>

   $(document).ready(function (){
       $('#confirmacao').modal('show');
   });
       
   

</script>

<div class="modal fade " id="confirmacao">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header" style="text-align: center;">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sair</span></button>
        <h3>Configuração realizada com sucesso!!!</h3>
      </div>
     
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }?>





<?php $msg = $this->Session->flash();
if($msg){
?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
    <?php echo $msg; ?>
</div>

<?php } ?>



<div class="panel panel-default">
                        <div class="panel-heading">
                            Definir configuração
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                
                                    
                                    
                                    <form method="post" role="form">
                                        <div class="form-group">
                                                <label for="">Definir UF de sistema : </label>
                                              
                                                    <?php echo $this->Form->input('Configuracao.uf_id',array('type'=>'select','options'=>$uf,'label'=>false,'div'=>false));?>
                                                
                                            </div>
                                        
                                        <button type="submit" class="btn btn-primary">Salvar</button>

                                    </form>
                                    
                                         
                                </div>
                            </div>
                        </div>
                    </div>

