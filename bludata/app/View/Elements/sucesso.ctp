<?php $msg = $this->Session->flash();
if($msg){
?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
    <?php echo $msg; ?>
</div>

<?php } ?>