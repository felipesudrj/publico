<?php $this->start('JavaScriptPaginas'); ?>
<?php echo $this->Html->script(array('jquery-ui')); ?>
<?php $this->end(); ?>

<?php echo $this->Html->css(array('jquery-ui.css')); ?>


<?php $msg = $this->Session->flash();
if ($msg) {
    ?>



    <div class="modal fade " id="confirmacao">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sair</span></button>
    <?php echo $msg; ?>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


<?php } ?>

    <div class="panel panel-default">
         <div class="panel-heading">
                             Lista de Clientes cadastrados
                        </div>
        
        <div class="panel-body">
            <form method="post">
                <label>Nome: </label>
                    
                <?php echo $this->FilterForm->input('nome_cliente', array('label' => false, 'type' => 'text')); ?>
                
                <label>Data Nascimento: </label>
                 <?php echo $this->FilterForm->input('data_nasc', array('label' => false, 'type' => 'text','class'=>'datetime')); ?>
                
                
                <label>Data de cadastro</label>
               <?php echo $this->FilterForm->input('data_cadastro', array('label' => false, 'type' => 'text','class'=>'datetime')); ?>

                
                <?php echo $this->FilterForm->submit('Buscar', array('class' => 'btn btn-primary',)); ?>
            </form>
           
            
        </div>
    </div>    
    
<div class="panel panel-default">
                        <div class="panel-heading">
                             Lista de Clientes cadastrados
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>CPF</th>
                                            <th>RG</th>
                                            <th>Data Nasc</th>
                                            <th>Data de cadastro</th>
                                            <th>Telefone</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($clientes as $indice=>$arrConteudo){ ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $arrConteudo['Cliente']['nome_cliente']?></td>
                                            <td><?php echo $this->Funcoes->mask($arrConteudo['Cliente']['cpf'],'###.###.###-##')?></td>
                                            <td><?php echo $arrConteudo['Cliente']['rg']?></td>
                                            <td class="center"><?php echo date('d/m/Y',  strtotime($arrConteudo['Cliente']['data_nasc']))?></td>
                                             <td class="center"><?php echo date('d/m/Y H:i:s',  strtotime($arrConteudo['Cliente']['data_cadastro']))?></td>
                                            <td class="center">
                                                
                                                <i class="fa fa-search" data-toggle="modal" data-target="#myModal<?php echo $arrConteudo['Cliente']['cliente_id']?>">
Visualizar
</i>

<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $arrConteudo['Cliente']['cliente_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Telefones</h4>
      </div>
      <div class="modal-body">
        
           <?php foreach ($arrConteudo['Telefone'] as $chave => $valor): ?>
                        <div class="form-group" id="foneId<?php echo $valor['telefone_id'] ?>">
                            (<?php echo $valor['ddd'] ?>)<?php echo $valor['numero'] ?> 
                        </div>
                    <?php endforeach; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
       
      </div>
    </div>
  </div>
</div>
                                                
                                                
                                                
                                                
                                            </td>
                                            <td class="center">
                                                <?php echo $this->Html->link('',array('controller'=>'Cliente','action'=>'novo',$arrConteudo['Cliente']['cliente_id']),array('class'=>'fa fa-edit','style'=>'color:black'))?>
                                                <?php echo $this->Html->link('',array('controller'=>'Cliente','action'=>'remove',$arrConteudo['Cliente']['cliente_id']),array('class'=>'fa fa-times','style'=>'color:black'))?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="7">
                                                
                                                <?php echo $this->Paginator->numbers();?>
                                                
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        </div>
                    </div>
    
    <script>
    $(document).ready(function(){
        $('#confirmacao').modal('show');
        $('.datetime').datepicker({dateFormat: 'dd/mm/yy',
                changeYear: true,
                changeMonth: true});
    })  
    
    
    </script>