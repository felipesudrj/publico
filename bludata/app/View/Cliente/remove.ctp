<div class="row">
    <div class="col-md-2">
        <strong> Nome:</strong>
    </div>
    <div class="col-md-4">
        <?php echo $cliente['Cliente']['nome_cliente']; ?>
    </div>
</div>


<div class="row">
    <div class="col-md-2">
        <strong> CPF:</strong>
    </div>
    <div class="col-md-4">
        <?php echo $cliente['Cliente']['cpf']; ?>
    </div>
</div>


<div class="row">
    <div class="col-md-2">
        <strong>  RG:</strong>
    </div>
    <div class="col-md-4">
        <?php echo $cliente['Cliente']['rg']; ?>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <strong> Data Nasc:</strong>
    </div>
    <div class="col-md-4">
        <?php echo date('d/m/Y', strtotime($cliente['Cliente']['data_nasc'])); ?>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        Tem certeza que seja remover?
        <form method="post">
            <div class="col-md-2">
                <?php echo $this->Form->input('cliente_id', array('type' => 'hidden', 'value' => $cliente['Cliente']['cliente_id'])) ?>
                <?php echo $this->Form->submit('Excluir', array('class' => 'btn btn-danger')); ?>
            </div>
            <div class="col-md-1">
                <?php echo $this->Html->link('Cancelar', array('action' => 'lista'), array('class' => 'btn btn-primary')); ?>
            </div>    


        </form>
    </div>

</div>