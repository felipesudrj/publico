<?php $this->start('JavaScriptPaginas'); ?>
<?php echo $this->Html->script(array('jquery.maskedinput.min', 'jquery-ui')); ?>
<?php $this->end(); ?>

<?php echo $this->Html->css(array('jquery-ui.css')); ?>
<script>
    function removeFone(chave) {
        $.ajax({
            url: "/cliente/removeFone",
            type: 'POST',
            data: 'id_fone='+chave,
            dataType: 'json',
            success: function(retorno) {
                if(retorno.status){
                    $('#foneId'+chave).remove();
                }else{
                   
                }
            }
        })
    }


    function removeTelefone(obj) {
        console.log(obj);
        $(obj).parent().parent().remove();

    }

    function inserirTelefone() {
        $('#telefone').append("<div class=\"form-group ntelefone row\"><div class=\"col-md-3\"><label>Numero: </label></div><div class=\"col-md-5\"><input type=\"text\" name=\"data[Telefone][][numero]\" class=\"mtelefone\"/> <i class=\"fa fa-times\" onclick=\"removeTelefone(this)\"></i></div></div>");
        $(".mtelefone").mask("(99)9999-999?9");
    }
    $(document).ready(function() {

<?php if ($conf == '1') { ?>
            var data = new Date();
            var ano = data.getFullYear() - 18;
            var options = {dateFormat: 'dd/mm/yy',
                changeYear: true,
                changeMonth: true
            };
<?php } else { ?>

            var data = new Date();
            
            
            var dia = data.getDate();
            var mes = data.getMonth();
            var ano = data.getFullYear() - 18;
            
            DataLimite = ano+"-"+mes+"-"+dia;
            
            var DataLimite = ano+'-'+mes+'-'+dia;
            var options = {dateFormat: 'dd/mm/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: new Date(DataLimite)
            };

<?php } ?>

        $('#confirmacao').modal('show');

        $("#datnasc").datepicker(options).attr('readonly', 'readonly');
        ;

        $("#cpf").mask("999.999.999-99");

        $('.enviar').click(function() {
            validado = true;
            $('.obrigatorio').each(function(k, v) {
                if ($(v).val().length == '0') {
                    validado = false;
                    $(this).css({"border-color": "#F00"});
                } else {
                    $(this).css({"border-color": ""});
                }
                ;

            });

            if (validado) {
                $('#cadastro').submit();
            }
        });

        $('#inserirtelefone').click(function() {
            inserirTelefone();


        });

    })</script>

<?php $msg = $this->Session->flash();
if ($msg) {
    ?>


    <div class="modal fade " id="confirmacao">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sair</span></button>
    <?php echo $msg; ?>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


<?php } ?>


<div class="panel panel-default">
    <div class="panel-heading">
        Cadastro de novo cliente   <span class="badge pull-right">
<?php echo ($conf == '1') ? "Base SC" : "Base PR"; ?>
        </span>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">



                <form method="post" role="form" id="cadastro">
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="">Nome : </label>
                        </div>
                        <div class="col-md-4">
                            <?php
                            if (isset($cliente_id)) {
                                echo $this->Form->input('Cliente.cliente_id', array('type' => 'hidden', 'label' => false, 'div' => false));
                            }
                            ?>
<?php echo $this->Form->input('Cliente.nome_cliente', array('type' => 'text', 'label' => false, 'div' => false)); ?>
                        </div>    
                    </div>


                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="">CPF : </label>
                        </div>
                        <div class="col-md-4">
<?php echo $this->Form->input('Cliente.cpf', array('id' => 'cpf', 'type' => 'text', 'label' => false, 'div' => false)); ?>
                        </div>    
                    </div>


                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="">Data Nasc : </label>
                        </div>
                        <div class="col-md-4">
<?php echo $this->Form->input('Cliente.data_nasc', array('id' => 'datnasc', 'type' => 'text', 'label' => false, 'div' => false)); ?>
                        </div>    
                    </div>



                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="">RG : </label><?php if ($conf == '1') { ?>
                                *
                            <?php } ?>
                        </div>
                        <div class="col-md-4">
<?php echo $this->Form->input('Cliente.rg', array('id' => 'rg', 'type' => 'text', 'class' => ($conf == '1') ? 'obrigatorio' : '', 'label' => false, 'div' => false)); ?>

                        </div>    
                    </div>

                    <div class="form-group ">
                        <label for="">Adicionar telefone : </label>

                        <i class="fa fa-plus-circle" id="inserirtelefone"></i>


                    </div>

                    <div class="" id="telefone">

<?php ?>



                    </div>

                    <button type="button" class="btn enviar btn-primary">Salvar</button>


                </form>


            </div>

            <div class="col-md-4">

<?php if (isset($cliente_id)) { ?>
                
                    <?php foreach ($this->data['Telefone'] as $chave => $valor): ?>
                        <div class="form-group" id="foneId<?php echo $valor['telefone_id'] ?>">
                            (<?php echo $valor['ddd'] ?>)<?php echo $valor['numero'] ?> <i class="fa fa-times" onclick="removeFone('<?php echo $valor['telefone_id'] ?>')"></i>
                        </div>
                    <?php endforeach; ?>
                
<?php } ?>


<?php if ($conf == '1') { ?>
                    <div class="pull-right">
                        * Campos obrigatórios
                    </div>

<?php } ?>
            </div>


        </div>
    </div>
</div>
