
<?php foreach ($clientes as $indice=>$arrConteudo){ ?>
<div class="col-md-12" style="border-bottom: 2px solid;margin: 20px;padding-bottom: 20px;">
    <div class="col-md-5">
<div class="row">
    
    <div class="col-md-4">
        <strong>Nome:</strong>
    </div>
    
    <div class="col-md-4">
        <strong><?php echo $arrConteudo['Cliente']['nome_cliente'];?></strong>
    </div>
    
</div>

<div class="row">
    
    <div class="col-md-4">
        <strong>CPF:</strong>
    </div>
    
    <div class="col-md-4">
        <strong><?php echo $arrConteudo['Cliente']['cpf'];?></strong>
    </div>
    
</div>


<div class="row">
    
    <div class="col-md-4">
        <strong>RG:</strong>
    </div>
    
    <div class="col-md-4">
        <strong><?php echo $arrConteudo['Cliente']['rg'];?></strong>
    </div>
    
</div>

<div class="row">
    
    <div class="col-md-4">
        <strong>Data Nascimento:</strong>
    </div>
    
    <div class="col-md-4">
        <strong><?php echo date('d/m/Y',  strtotime($arrConteudo['Cliente']['data_nasc']));?></strong>
    </div>
    
</div>
</div>
    <div class="col-md-6">
        Telefones<br>
        
        <?php foreach ($arrConteudo['Telefone'] as $chave => $valor): ?>
                        <div class="form-group" id="foneId<?php echo $valor['telefone_id'] ?>">
                            (<?php echo $valor['ddd'] ?>)<?php echo $valor['numero'] ?> 
                        </div>
                    <?php endforeach; ?>
    </div>
</div>
<br>
<hr>
<?php } ?>